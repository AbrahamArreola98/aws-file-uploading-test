const AWS = require('aws-sdk');
const fs = require('fs');

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

const filePath = 'index.css';
const fileName = 'index.css';
const fileContent = fs.readFileSync(filePath);

const params = {
    Bucket: process.env.AWS_BUCKET_NAME,
    Key: fileName,
    Body: fileContent,
    ACL: 'public-read',
    ContentType: "text/css"
};

s3.upload(params, function (err, data) {
    if (err) {
        throw err;
    }
    console.log(`File uploaded successfully: ${data.Location}`);
});